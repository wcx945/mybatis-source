package com.wcx.mybatis;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import com.wcx.mybatis.dao.BlogMapper;
import com.wcx.mybatis.dao.BlogMapperImpl;
import com.wcx.mybatis.entity.Blog;

import sun.misc.ProxyGenerator;

public class ProxyHandle {
	
	public static void main(String[] args) {
//		BlogMapper mapper = new BlogMapperImpl();
//		BlogMapper proxy = (BlogMapper)((new BlogMapperProxy(mapper)).getProxy());
//		Blog blog = new Blog();
//		proxy.insert(blog);
		
		String path = "C:/opt/$Proxy0.class";  
        byte[] classFile = ProxyGenerator.generateProxyClass("$Proxy0",  
        		BlogMapperImpl.class.getInterfaces()); 
        try {
			OutputStream os = new FileOutputStream(path);
			os.write(classFile);
			os.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		
	}
}
