package com.wcx.mybatis;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.wcx.mybatis.dao.BlogMapper;
import com.wcx.mybatis.entity.Blog;

public class MyApplication {
	
	public static void main(String[] args) throws IOException {
		
		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		
		SqlSession session = sqlSessionFactory.openSession();
		BlogMapper mapper = session.getMapper(BlogMapper.class);
		List<Blog> blogs = new ArrayList<Blog>();
		for(int i=0;i<2;i++){
			Blog blog = new Blog();
			blog.setPassword("wcx"+i);
			blog.setUserName("wcx"+i);
			blogs.add(blog);
		}
		insertBatch(mapper,blogs);
		session.commit();
	}
	
	public static void insert(BlogMapper mapper,Blog blog){
		mapper.insert(blog);
		
	}
	
	public static void insertBatch(BlogMapper mapper,List<Blog> blogs){
		mapper.insertBatch(blogs);
		
	}
}
