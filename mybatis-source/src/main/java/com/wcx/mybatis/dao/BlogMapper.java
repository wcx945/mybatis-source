package com.wcx.mybatis.dao;

import java.util.List;
import java.util.Map;

import com.wcx.mybatis.entity.Blog;

public interface BlogMapper {
	
	  Blog selectBlogUser(Map<String,Object> map);
	  
	  int insert(Blog blog);
	  
	  int insertBatch(List<Blog> blogs);
}