package com.wcx.mybatis;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class BlogMapperProxy implements InvocationHandler{
	
	Object target;
	
	BlogMapperProxy(Object target){
		this.target = target;
	}

	public Object invoke(Object arg0, Method arg1, Object[] arg2) throws Throwable {
		System.out.println("proxy");
		arg1.invoke(target,arg2);
		return null;
	}
	
	public Object getProxy(){
		return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), target.getClass().getInterfaces(), this);
	}

}
